package com.example.fullstackapi;

import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@SpringBootApplication
public class MainController {

		static RestTemplate restTemplate;

		public MainController(){
		restTemplate = new RestTemplate();
		}

	public static void main(String[] args) {
		SpringApplication.run(MainController.class, args);
	}
}
