package com.example.fullstackapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class HospitalController {


    @Autowired
    private HospitalService hospitalService;


    @RequestMapping("/hospitals/{id}")
    public @ResponseBody Hospital getHospital(@PathVariable("id") int id) throws Exception {
        return this.hospitalService.getHospital(id);
    }
    @RequestMapping("/hospitals")
    public @ResponseBody List<Hospital> getAllHospitals() throws Exception {
        return this.hospitalService.getAllHospitals();
    }
    @RequestMapping(method= RequestMethod.POST, value="/hospitals")
    public ResponseEntity<String> addHospital(@RequestBody Hospital hospital) {
        this.hospitalService.addHospital(hospital);
        return new ResponseEntity<>("data saved", HttpStatus.OK);
    }


    @RequestMapping(method=RequestMethod.PUT, value="/hospitals/{id}")
    public ResponseEntity<String> updateHospital(@RequestBody Hospital hospital, @PathVariable int id) {
        this.hospitalService.updateHospital(hospital, id);
        return new ResponseEntity<>("data saved", HttpStatus.OK);
    }


    @RequestMapping(method=RequestMethod.DELETE, value="/hospitals/{id}")
    public ResponseEntity<String> deleteHospital(@PathVariable int id) {
        this.hospitalService.deleteHospital(id);
        return new ResponseEntity<>("data deleted", HttpStatus.OK);
    }


}
