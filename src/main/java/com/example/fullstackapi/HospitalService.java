package com.example.fullstackapi;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Service;


@Service
public class HospitalService {

    private List<Hospital> hospitalList=new ArrayList<>(Arrays.asList(
            new Hospital(1001, "Apollo Hospital", "Chennai", 3.8),
            new Hospital(1002,"Global Hospital","Chennai", 3.5),
            new Hospital(1003,"VCare Hospital","Bangalore", 3)));


    public List<Hospital> getAllHospitals(){
        return hospitalList;
    }
    public Hospital getHospital(int id){
        return hospitalList.stream().filter(c->c.getId()==(id)).findFirst().get();
    }
    public void addHospital(Hospital hospital){
        hospitalList.add(hospital);
    }


    public void updateHospital(Hospital category, int id){
        for(int i=0;i<hospitalList.size();i++){
            Hospital c= hospitalList.get(i);
            if(c.getId()==id){
                hospitalList.set(i, category);
                return;
            }
        }
    }


    public void deleteHospital(int id){
        hospitalList.remove(hospitalList.stream().filter(c->c.getId()==(id)).findFirst().get());
    }


}


