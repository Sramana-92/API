package com.example.fullstackapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;


@RestController
public class NewsController {

    @Autowired
    NewsService newsService;

    @RequestMapping("/top")
    public  JSONObject getNews() throws Exception{
        JSONObject books=newsService.getTopStories();
        System.out.println(books);
        return books;

    }
}

