package com.example.fullstackapi;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class NewsService {

    static RestTemplate restTemplate;

    public NewsService(){
        restTemplate = new RestTemplate();
    }

    public static JSONObject getTopStories() throws Exception{
        JSONObject books = new JSONObject();
        String getUrl = "https://api.nytimes.com/svc/topstories/v2/arts.json?api-key=6ytpyq5ZPu7COgc830714UoG8GPw5JWb";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<Map> bookList = restTemplate.exchange(getUrl, HttpMethod.GET, entity, Map.class);
        if (bookList.getStatusCode() == HttpStatus.OK) {
            books = new JSONObject(bookList.getBody());
        }
        return books;
    }


}

